import com.alibaba.fastjson.JSONObject;
import com.lgq.common.Util.TokenUtil;
import com.lgq.common.Util.UserUtil;
public class test {
    public static void main(String[] args) throws Exception {
         System.out.println("生成token:" + TokenUtil.getTokenStr(new UserUtil().getUser("张三", "112345")));
        //生成token: eyJhbGciOiJBRVMiLCJ0eXAiOiJKV1QifQ==.eyJleHAiOiIxMDgwMDAwMCIsImlhdCI6IjE2OTQ5NDYyMzQ0NjciLCJpc3MiOiJ3d3cubGl1Z3VpcWluZy5jb20iLCJ1c2VyRGF0YSI6e319.K1p4NnNiM2lKWmFNOEh1cFkzcmdTc3VvN1pIbEZtdEFNSVNSOUMzYmtGMmRmZ1ZMZnBWSWNzd1BXVk9ha1R2cEhYTjlBdjJnUTVVSg0KOTNhZis5bDM4d2R1K1dUamhmUWpMbk54Y0pNUnN3UldnMXhOcmRDR1FjNzM1aU45T2xxNWdVZ3VWdEcwK2FiajhjZjlxektmTmc9PQ==
        // 解密：校验token
        String tokenstr = "eyJhbGciOiJBRVMiLCJ0eXAiOiJKV1QifQ==.eyJleHAiOiIxMDgwMDAwMCIsImlhdCI6IjE2OTQ5NDYyMzQ0NjciLCJpc3MiOiJ3d3cubGl1Z3VpcWluZy5jb20iLCJ1c2VyRGF0YSI6e319.K1p4NnNiM2lKWmFNOEh1cFkzcmdTc3VvN1pIbEZtdEFNSVNSOUMzYmtGMmRmZ1ZMZnBWSWNzd1BXVk9ha1R2cEhYTjlBdjJnUTVVSg0KOTNhZis5bDM4d2R1K1dUamhmUWpMbk54Y0pNUnN3UldnMXhOcmRDR1FjNzM1aU45T2xxNWdVZ3VWdEcwK2FiajhjZjlxektmTmc9PQ==";
                System.out.println(TokenUtil.tokenCheck(tokenstr, new UserUtil().getUser("张", "112345")));
        /*
         *输出： {"msg":"用户不存在，请重新输入。","code":"500"}
         *
         *
         * 此用户信息：logger日志
         * token中的用户信息：uname: 张三,upw: 112345,
         * 提交的用户信息：udate_uname: 张,udate_upw: 112345
         * */
        System.out.println("-----------");
        System.out.println(TokenUtil.tokenCheck(tokenstr, new UserUtil().getUser("张三", "123456")));
        /*
         *输出：{"msg":"用户密码错误！请重新输入。","code":"500"}
         *
         *
         * 此用户信息：logger日志
         * token中的用户信息：uname: 张三,upw: 112345,
         * 提交的用户信息：udate_uname: 张三,udate_upw: 123456
         * */
        System.out.println("-----------");
        JSONObject jsonObject = TokenUtil.tokenCheck(tokenstr, new UserUtil().getUser("张三", "112345"));
        System.out.println(jsonObject);
        /*
         *输出：{"msg":"用户信息验证成功！","code":"200","token":"true"}
         *
         *
         * 此用户信息：logger日志
         * token中的用户信息：uname: 张三,upw: 112345,
         * 提交的用户信息：udate_uname: 张三,udate_upw: 112345
         * */

        //用户信息校验：
        System.out.println(TokenUtil.tokenCheck(jsonObject));//true  用户名及密码存在且正确且不过期，返回true,否则为false;
    }
}

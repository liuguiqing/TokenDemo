package com.lgq.common.entity;

import java.util.Map;

/**
 * Playload（荷载）：
 */
public class PlayloadTokenEntity {
    /**
     * 签发人
     */
    public String iss;
    /**
     * 过期时间，时间戳
     */
    public String exp;
    /**
     * 签发时间 时间戳
     */
    public String iat;
    /**
     * 主体
     */
    public String sub;
    /**
     * 接收方
     */
    public String aud;

    /**
     * 用户登录数据（用户名及密码）
     */
    public Map userData;

    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getIat() {
        return iat;
    }

    public void setIat(String iat) {
        this.iat = iat;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getAud() {
        return aud;
    }

    public void setAud(String aud) {
        this.aud = aud;
    }

    public Map getUserData() {
        return userData;
    }

    public void setUserData(Map userData) {
        this.userData = userData;
    }
}

package com.lgq.common.entity;

/**
 * Header（加密方式）：
 */
public class HeaderTokenEntity {
    /**
     * 类型：
     */
    public String typ;
    /**
     * 加密方法名：
     */
    public String alg;

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getAlg() {
        return alg;
    }

    public void setAlg(String alg) {
        this.alg = alg;
    }
}

package com.lgq.common.Util;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;

/**
 * Base64加密解密
 */
public class Base64Util {

    /**
     * 加密：
     *
     * @param key
     * @return
     */
    public static String encryptBASE64(byte[] key) {
        return new BASE64Encoder().encodeBuffer(key);
    }

    /**
     * 解密：
     *
     * @param key
     * @return
     */
    public static byte[] decryptBASE64(String key) {
        try {
            return new BASE64Decoder().decodeBuffer(key);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

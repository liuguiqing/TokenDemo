package com.lgq.common.Util;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


/**
 * AES加密解密
 */
public class AesUtil {
    /**
     * 用户密码自定义加密盐：
     */
    public static final String user_pw_key = "oia3i4iJHU*$%#;[-_slk1233"; //实际上配置在application配置文件中，这里动态获取。不打包在jar里，此盐为测试数据。
    public static final String charset = "utf-8";
    public static final String AES = "AES";

    /**
     * AES主加密方法
     *
     * @param key_salt 自定义加密盐
     * @param content  需要加密的明文
     * @return
     */
    public static String encryptAES(String content, String key_salt) {
        try {
            KeyGenerator keygen = KeyGenerator.getInstance(AES);
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(key_salt.getBytes());
            keygen.init(128, random);
            SecretKey original_key = keygen.generateKey();
            byte[] raw = original_key.getEncoded();
            SecretKey key = new SecretKeySpec(raw, AES);
            Cipher cipher = Cipher.getInstance(AES);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] byte_encode = content.getBytes(charset);
            byte[] byte_AES = cipher.doFinal(byte_encode);
            String AES_encode = new String(new BASE64Encoder().encode(byte_AES));
            return AES_encode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * AES主解密方法
     *
     * @param key_salt 自定义加密盐
     * @param content  需要解密的密文
     * @return
     */
    public static String decryptAES(String content, String key_salt) {
        try {
            KeyGenerator keygen = KeyGenerator.getInstance(AES);
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(key_salt.getBytes());
            keygen.init(128, random);
            SecretKey original_key = keygen.generateKey();
            byte[] raw = original_key.getEncoded();
            SecretKey key = new SecretKeySpec(raw, AES);
            Cipher cipher = Cipher.getInstance(AES);
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] byte_content = new BASE64Decoder().decodeBuffer(content);
            byte[] byte_decode = cipher.doFinal(byte_content);
            String AES_decode = new String(byte_decode, charset);
            return AES_decode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}


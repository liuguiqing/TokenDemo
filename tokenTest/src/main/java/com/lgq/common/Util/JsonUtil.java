package com.lgq.common.Util;

import com.alibaba.fastjson.JSONObject;

/**
 * json工具
 */
public class JsonUtil {
    /**
     * 验证json中的key是否存在
     *
     * @param jsonObj jsonObject
     * @param key
     * @return 存在， 返回true
     */
    public static boolean isNotEmpty(JSONObject jsonObj, String key) {
        if (null != jsonObj.get(key) && jsonObj.containsKey(key)) {
            return true;
        } else {
            return false;
        }
    }
}

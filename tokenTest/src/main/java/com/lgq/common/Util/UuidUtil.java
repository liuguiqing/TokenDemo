package com.lgq.common.Util;

import java.util.UUID;

/**
 * get uuid
 */
public class UuidUtil {
    public static String getUuid() {
        String uuid= UUID.randomUUID().toString()
                .replace("-","");
        return uuid;
    }
}

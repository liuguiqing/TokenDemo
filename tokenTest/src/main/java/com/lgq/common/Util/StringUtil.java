package com.lgq.common.Util;

import java.util.Map;

/**
 * 字符串处理：
 */
public class StringUtil {

    /**
     * 多行改一行，去掉换行
     *
     * @param str 待处理字符串
     * @return 返回一行字符串 （去掉换行\r、回车\n、制表\t）
     */
    public static String getStrToARow(String str) {
        if (str != null && !str.isEmpty()) {
            return str.replaceAll("[\t\n\r]", "");
        } else {
            return str;
        }
    }

    /**
     * 判断集合中key是否存在
     *
     * @param map Map
     * @param key
     * @return 存在返回true
     */
    public static boolean isNotEmpty(Map map, String key) {
        if (null != map.get(key) && map.containsKey(key)) {
            return true;
        } else {
            return false;
        }
    }

}

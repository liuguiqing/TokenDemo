package com.lgq.common.Util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lgq.common.entity.HeaderTokenEntity;
import com.lgq.common.entity.PlayloadTokenEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Token令牌-生成工具
 * 头部（header)+载荷（payload)+签证（signature)
 */

public class TokenUtil {

    //Logger logger = LoggerFactory.getLogger(TokenUtil.class);
    /**
     * 自定义加密盐
     */
    public static final String TokenSelf_SaltKey = "Ldeks./,'[]-=&^&Hjkdyye";//实际上配置在application配置文件中，这里动态获取。不打包在jar里，此盐为测试数据。

    public static final String Token_TYP = "JWT";
    public static final String Token_ALG = "AES";
    /**
     * 过期时间（毫米）， 3小时后过期
     */
    public static final String Token_EXP = "10800000";
    /**
     * 签发人
     */
    public static final String Token_ISS = "www.liuguiqing.com";

    /**
     * 生成token令牌
     *
     * @param udata 用户登录数据
     * @return string
     * @throws Exception
     */
    public static String getTokenStr(Map udata) throws Exception {
        PlayloadTokenEntity userDataP = new PlayloadTokenEntity();
        userDataP.setUserData(udata);
        String udp = getJWT(userDataP);
        return udp;
    }

    /**
     * header生成jwt（header加入信息转json再Base64加密）
     *
     * @return
     * @throws Exception
     */
    private static String HeaderTokenToBase64() throws Exception {
        HeaderTokenEntity headerTE = new HeaderTokenEntity();
        headerTE.setTyp(Token_TYP);
        headerTE.setAlg(Token_ALG);
        String header_Json = JSON.toJSONString(headerTE);
        String headerToBase64 = Base64Util.encryptBASE64(header_Json.getBytes());
        return headerToBase64;
    }

    /**
     * payload生成jwt
     * 1、payload添加信息（不含用户信息）。
     * 2、转json。
     * 3、Base64加密。
     *
     * @param playload
     * @return
     */
    private static String PayloadTokenToBase64(PlayloadTokenEntity playload) {
        playload.setIss(Token_ISS);
        playload.setExp(Token_EXP);
        playload.setIat(String.valueOf(System.currentTimeMillis()));
        //用户信息置空：
        playload.setUserData(new HashMap());
        String playload_Json = JSON.toJSONString(playload);
        String playloadToBase64 = Base64Util.encryptBASE64(playload_Json.getBytes());
        return playloadToBase64;
    }

    /**
     * 生成JWT Token串
     * 1、Header、Payload基础数据base64加密
     * 2、用户信息base64加密后再AES加盐&&加密
     * 3、返回三者加 . 的token串。
     *
     * @return
     */
    public static String getJWT(PlayloadTokenEntity tokenPlayload) throws Exception {
        try {
            //logger.info("-----------------token start-----------------");
            String userStr = JSON.toJSONString(tokenPlayload);
            //logger.info("getJWT-userStr: "+userStr);

            //获取Header、Payload的Base64加密后的数据：
            String HeaderBase64 = HeaderTokenToBase64();
            String PayloadBase64 = PayloadTokenToBase64(tokenPlayload);//不含用户信息
            //基本数据中间加 ‘.’
            StringBuilder dotSB = new StringBuilder();
            dotSB.append(HeaderBase64);
            dotSB.append(".");
            dotSB.append(PayloadBase64);
            dotSB.append(".");

            //用户数据AES加盐加密：
            String signature = Base64Util.encryptBASE64(AesUtil.encryptAES(userStr, TokenSelf_SaltKey).getBytes());
            dotSB.append(signature);
            //去掉换行：
            String token = StringUtil.getStrToARow(dotSB.toString());
            //logger.info("-----------------token end-----------------");
            return token;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 验证token加密串合法性
     *
     * @param token 加密串
     * @return
     */
    public static boolean TokenTF(String token) {
        if (token.indexOf(".") > -1 && token.split("\\.").length == 3) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 校验token
     *
     * @param tokenstr token加密串
     * @return 用户校验信息
     */
    public static JSONObject tokenCheck(String tokenstr, Map udata) {
        JSONObject jsonObj = new JSONObject();
        try {
            if (TokenTF(tokenstr)) {
                String[] tokens = tokenstr.split("\\.");
                String tokenPayload = new String(Base64Util.decryptBASE64(tokens[1]));
                String tokenUser = new String(Base64Util.decryptBASE64(tokens[2]));
                String userInfo = AesUtil.decryptAES(tokenUser, TokenSelf_SaltKey);
                jsonObj=UserUtil.userInfoCheck(tokenPayload,userInfo,udata);
            }else{
                jsonObj.put("token","false");
                jsonObj.put("code","500");
                jsonObj.put("msg","用户信息不存在！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }


    public static boolean tokenCheck(JSONObject json) {
        if (JsonUtil.isNotEmpty(json, "token")) {
            return json.getString("token").equals("true");
        } else {
            return false;
        }
    }


}

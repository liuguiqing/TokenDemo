package com.lgq.common.Util;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UserUtil {
    /**
     * 用户信息封装到map中
     *
     * @param uname 用户名
     * @param upw   用户密码
     * @return
     */
    public Map getUser(String uname, String upw) {
        Map map = new HashMap();
        map.put("uuid", UuidUtil.getUuid());
        map.put("uname", uname);
        map.put("upw", AesUtil.encryptAES(upw, AesUtil.user_pw_key));
        return map;
    }

    /**
     * 用户信息验证：
     *
     * @param tokenPayload tokenPayload
     * @param userInfo     token中的用户信息
     * @param umap         输入的用户信息
     * @return
     */
    public static JSONObject userInfoCheck(String tokenPayload, String userInfo, Map umap) {
        JSONObject jsonObject = new JSONObject();
        try {
            //获取token中签证时间和过期时间：
            JSONObject jsonPay = JSONObject.parseObject(tokenPayload);
            Long dateall = null;
            if (JsonUtil.isNotEmpty(jsonPay, "iat") && JsonUtil.isNotEmpty(jsonPay, "exp")) {
                dateall = Long.valueOf(jsonPay.getString("iat")) + Long.valueOf(jsonPay.getLong("exp"));
            }
            //当前时间，时间戳：
            Long asDate = System.currentTimeMillis();
            //获取token中用户信息：
            JSONObject jsonUser = JSONObject.parseObject(userInfo);
            JSONObject uinfo = jsonUser.getJSONObject("userData");
            if (!JsonUtil.isNotEmpty(uinfo, "uname")) {
                jsonObject.put("code", "500");
                jsonObject.put("msg", "用户不存在！");
            }
            //获取token解密后的用户信息：
            String uname = "", upw = "";
            if (JsonUtil.isNotEmpty(uinfo, "upw") && JsonUtil.isNotEmpty(uinfo, "uname")) {
                uname = uinfo.getString("uname");
                upw = AesUtil.decryptAES(uinfo.getString("upw"), AesUtil.user_pw_key);
            }
            //获取登录的用户信息：
            String udate_uname = "", udate_upw = "";
            if (StringUtil.isNotEmpty(umap, "uname") && StringUtil.isNotEmpty(umap, "upw")) {
                udate_uname = umap.get("uname").toString();
                udate_upw = AesUtil.decryptAES(umap.get("upw").toString(), AesUtil.user_pw_key);
            }
            //logger.info("uname: "+uname+",upw: "+upw+",udate_uname: "+udate_uname+",udate_upw: "+udate_upw);
            if (!uname.equals(udate_uname)) {
                jsonObject.put("code", "500");
                jsonObject.put("msg", "用户不存在，请重新输入。");
            }
            if (uname.equals(udate_uname) && !upw.equals(udate_upw)) {
                jsonObject.put("code", "500");
                jsonObject.put("msg", "用户密码错误！请重新输入。");
            }
            if (uname.equals(udate_uname) && upw.equals(udate_upw)) {
                if (dateall < asDate) {
                    jsonObject.put("code", "500");
                    jsonObject.put("msg", "登录超时，请重新登录。");
                } else {
                    jsonObject.put("token", "true");
                    jsonObject.put("code", "200");
                    jsonObject.put("msg", "用户信息验证成功！");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}
